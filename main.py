# ///////////////////////////////////////////////////////////////
#
# BY: WANDERSON M.PIMENTA
# PROJECT MADE WITH: Qt Designer and PySide6
# V: 1.0.0
#
# This project can be used freely for all uses, as long as they maintain the
# respective credits only in the Python scripts, any information in the visual
# interface (GUI) can be modified without any implication.
#
# There are limitations on Qt licenses if you want to use your products
# commercially, I recommend reading them on the official website:
# https://doc.qt.io/qtforpython/licenses.html
#
# ///////////////////////////////////////////////////////////////

import sys
import os
import platform
import socket
import tqdm
import pyttsx3

# WEBCAM IMPORTS
import cv2
import datetime
from PyQt5 import QtGui, QtWidgets
from PyQt5.QtGui import QImage, QPixmap

# IMPORT / GUI AND MODULES AND WIDGETS
# ///////////////////////////////////////////////////////////////
from modules import *
from widgets import *
from phonon import *
from PyQt5.QtMultimediaWidgets import QVideoWidget
os.environ["QT_FONT_DPI"] = "96" # FIX Problem for High DPI and Scale above 100%

# GLOBAL VARS
HEADER = 64
PORT = 5050
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
BUFFER_SIZE = 4096
SEPARATOR = "<SEP>"
SERVER = "192.168.0.2"
ADDR = (SERVER, PORT)

# SET AS GLOBAL WIDGETS
# ///////////////////////////////////////////////////////////////
widgets = None

tts_engine = pyttsx3.init()

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        # SET AS GLOBAL WIDGETS
        # ///////////////////////////////////////////////////////////////
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        global widgets
        widgets = self.ui

        # USE CUSTOM TITLE BAR | USE AS "False" FOR MAC OR LINUX
        # ///////////////////////////////////////////////////////////////
        Settings.ENABLE_CUSTOM_TITLE_BAR = True

        # APP NAME
        # ///////////////////////////////////////////////////////////////
        title = "PyDracula - Modern GUI"
        description = "Image Inference to Text - Project IA"
        # APPLY TEXTS
        self.setWindowTitle(title)
        widgets.titleRightInfo.setText(description)
        widgets.lineEditIP.setText(str(SERVER));
        widgets.lineEditPort.setText(str(PORT));

        # TOGGLE MENU
        # ///////////////////////////////////////////////////////////////
        widgets.toggleButton.clicked.connect(lambda: UIFunctions.toggleMenu(self, True))

        # SET UI DEFINITIONS
        # ///////////////////////////////////////////////////////////////
        UIFunctions.uiDefinitions(self)

        # BUTTONS CLICK
        # ///////////////////////////////////////////////////////////////

        # LEFT MENUS
        #widgets.btn_home.clicked.connect(self.buttonClick)
        widgets.btn_webcam.clicked.connect(self.buttonClick)
        widgets.btn_inference.clicked.connect(self.buttonClick)

        # CAMERA BUTTONS
        widgets.ScreenShotspath = "screenshots"
        widgets.Inferencespath = "inferences"
        self.createDirectory(widgets.ScreenShotspath)
        self.createDirectory(widgets.Inferencespath)
        widgets.logic = 0
        widgets.SHOW.clicked.connect(self.ShowClicked)
        #widgets.TEXT.setText('Presiona "Abrir WebCam" para encender tu webcam')
        widgets.CAPTURE.clicked.connect(self.CaptureClicked)
        widgets.STOP.clicked.connect(self.StopClicked)
        widgets.CAPTURE.setEnabled(False)
        widgets.STOP.setEnabled(False)
        #widgets.btnUbication.clicked.connect(self.openDirectory)
        widgets.btnFile.clicked.connect(self.openImage)

        #SET INITIAL WEBCAMsetWebcam
        widgets.cap = cv2.VideoCapture(widgets.comboBoxWebCam.currentIndex())

        # EXTRA LEFT BOX
        def openCloseLeftBox():
            UIFunctions.toggleLeftBox(self, True)
        widgets.toggleLeftBox.clicked.connect(openCloseLeftBox)
        widgets.extraCloseColumnBtn.clicked.connect(openCloseLeftBox)

        # EXTRA RIGHT BOX
        def openCloseRightBox():
            UIFunctions.toggleRightBox(self, True)
        widgets.settingsTopBtn.clicked.connect(openCloseRightBox)

        # SHOW APP
        # ///////////////////////////////////////////////////////////////
        self.show()

        # SET CUSTOM THEME
        # ///////////////////////////////////////////////////////////////
        useCustomTheme = False
        themeFile = "themes\py_dracula_light.qss"

        # SET THEME AND HACKS
        if useCustomTheme:
            # LOAD AND APPLY STYLE
            UIFunctions.theme(self, themeFile, True)

            # SET HACKS
            AppFunctions.setThemeHack(self)

        # SET HOME PAGE AND SELECT MENU
        # ///////////////////////////////////////////////////////////////
        widgets.stackedWidget.setCurrentWidget(widgets.webcam_page)
        widgets.btn_webcam.setStyleSheet(UIFunctions.selectMenu(widgets.btn_webcam.styleSheet()))


    # BUTTONS CLICK
    # Post here your functions for clicked buttons
    # ///////////////////////////////////////////////////////////////
    def buttonClick(self):
        # GET BUTTON CLICKED
        btn = self.sender()
        btnName = btn.objectName()

        # SHOW WEBCAM PAGE
        if btnName == "btn_webcam":
            widgets.stackedWidget.setCurrentWidget(widgets.webcam_page) # SET PAGE
            UIFunctions.resetStyle(self, btnName) # RESET ANOTHERS BUTTONS SELECTED
            btn.setStyleSheet(UIFunctions.selectMenu(btn.styleSheet())) # SELECT MENU

        # SHOW INFERENCE PAGE
        if btnName == "btn_inference":
            widgets.stackedWidget.setCurrentWidget(widgets.inference_page)
            UIFunctions.resetStyle(self, btnName) 
            btn.setStyleSheet(UIFunctions.selectMenu(btn.styleSheet()))

        # PRINT BTN NAME
        print(f'Button "{btnName}" pressed!')


    # RESIZE EVENTS
    # ///////////////////////////////////////////////////////////////
    def resizeEvent(self, event):
        # Update Size Grips
        UIFunctions.resize_grips(self)

    # MOUSE CLICK EVENTS
    # ///////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        # SET DRAG POS WINDOW
        self.dragPos = event.globalPos()

        # PRINT MOUSE EVENTS
        if event.buttons() == Qt.LeftButton:
            print('Mouse click: LEFT CLICK')
        if event.buttons() == Qt.RightButton:
            print('Mouse click: RIGHT CLICK')

    # OPEN WEBCAM
    # //////////////////////////////////////////////////////////////
    def ShowClicked(self):
        widgets.TEXT.setText('Open webcam')
        widgets.video_size = QSize(650, 350)
        widgets.cap = cv2.VideoCapture(widgets.comboBoxWebCam.currentIndex())
        widgets.cap.set(cv2.CAP_PROP_FRAME_WIDTH, widgets.video_size.width())
        widgets.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, widgets.video_size.height())
        widgets.SHOW.setEnabled(False)
        widgets.CAPTURE.setEnabled(True)
        widgets.STOP.setEnabled(True)
        while(widgets.cap.isOpened()):
            ret, frame = widgets.cap.read()

            if ret == True:
                self.displayImage(frame)
                cv2.waitKey()

                if(widgets.logic == 2):
                    date = datetime.datetime.now()
                    IMAGENAME = (date.year, date.month, date.day, date.hour, date.minute, date.second)
                    NAME = widgets.ScreenShotspath+'/Image_%s%s%sT%s%s%s.png'%IMAGENAME
                    cv2.imwrite(NAME, frame)

                    widgets.logic = 1
                    widgets.TEXT.setText('Image saved')
                    
                    self.displayCapture(NAME)
            else:
                print('return not found')
        
        

    # CAPTURE SCREEN SHOT
    # //////////////////////////////////////////////////////////////
    def CaptureClicked(self):
        widgets.logic=2

    # CLOSE WEBCAM
    # //////////////////////////////////////////////////////////////
    def StopClicked(self):
        widgets.TEXT.setText('Closed webcam')
        widgets.cap.release()
        empty_img = QPixmap()
        widgets.imgLabel.setPixmap(empty_img.swap(QPixmap()))
        widgets.SHOW.setEnabled(True)
        widgets.CAPTURE.setEnabled(False)
        widgets.STOP.setEnabled(False)
        # cv2.destroyAllWindows()

    def closeEvent(self, event):
        widgets.cap.release()

    # DISPLAY WEBCAM IMAGE
    # //////////////////////////////////////////////////////////////
    def displayImage(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.flip(frame, 1)
        img = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
        img = QPixmap.fromImage(img)
        widgets.imgLabel.setPixmap(img)

    # DISPLAY SCREENSHOT
    # //////////////////////////////////////////////////////////////
    def displayCapture(self,URL):
        print(URL)
        #widgets.imgLabelPhoto.setPixmap(QPixmap(URL))
        widgets.inferenceWebcamText.setText("Running image detection...")
        
        send_file(URL, "webcam")

    # CREATE DIRECTORY SCREENSHOTS
    # //////////////////////////////////////////////////////////////
    def createDirectory(self,path):
        try:
            os.mkdir(path)
        except OSError:
            print ("Creation of the directory %s failed" % path)
        else:
            print ("Successfully created the directory %s " % path)

    # OPEN DIRECTORY SCREENSHOT
    # //////////////////////////////////////////////////////////////
    def openDirectory(self):
        directory = str(QFileDialog.getExistingDirectory())
        widgets.ScreenShotspath = '{}'.format(directory)
        print(widgets.ScreenShotspath)
        widgets.lineEditUbication.setText(widgets.ScreenShotspath)

    # OPEN IMAGE FILE
    # //////////////////////////////////////////////////////////////
    def openImage(self):
        dialog = QFileDialog.getOpenFileName(None, "Select a file...",'./', filter="Image Files(*.png *.jpg *.bmp)")
        widgets.lineEditFile.setText(dialog[0])
        widgets.labelImage.setPixmap(QPixmap(dialog[0]))
        widgets.inferenceText.setText("Running image detection...")
        
        send_file(dialog[0], "image")

def send_file(input_file, method_from):
    SERVER = widgets.lineEditIP.text()
    PORT = widgets.lineEditPort.text()
    ADDR = (SERVER, PORT)
    
    # Get info of the file 
    filesize = os.path.getsize(input_file)
    input_name, input_ext = os.path.splitext(input_file)

    
    #Create socket an connect with the server
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(ADDR)
    #Send initial info to server
    encoded_info = f"{input_ext}{SEPARATOR}{filesize}".encode(FORMAT)
    client.send(encoded_info)
    # Send file to server
    progress = tqdm.tqdm(range(filesize), f"Sending {input_file}", unit = "B", unit_scale = True, unit_divisor = 1024)
    #Read the file in buffer sized chunks and send them to server until end of file
    with open(input_file, "rb") as f:
        bytes_read = f.read(BUFFER_SIZE)
        while bytes_read: 
            client.sendall(bytes_read)
            progress.update(len(bytes_read))
            bytes_read = f.read(BUFFER_SIZE)
        f.close()
        progress.close()
    #Waiting for result of conversion
    print("Waiting for conversion...")
    response = client.recv(BUFFER_SIZE).decode(FORMAT)
    success, r_filesize, info = response.split(SEPARATOR)
    success = bool(success)
    tts_text = ""
    if success:
        print(info)
        #Receive buffer sized chunks of the converted file and write them
        filesize = int(r_filesize)
        filename = os.path.splitext(os.path.basename(input_file))[0]
        progressR = tqdm.tqdm(range(filesize), f"Receiving inference result", unit = "B", unit_scale = True, unit_divisor = 1024)
        inference_path = f"./{widgets.Inferencespath}/{filename}_inference{input_ext}"
        with open(inference_path, "wb") as f:
            received_data = 0
            while received_data < filesize:
                bytes_read = client.recv(BUFFER_SIZE)
                # write to the file the bytes we just received
                f.write(bytes_read)
                # update the progress bar
                received_data += len(bytes_read)
                progressR.update(len(bytes_read))
            f.close()
            progressR.close()
            print("File transfer done...")
        if method_from == "image":
            widgets.inferenceText.setText(info)
            widgets.inferenceImage.setPixmap(QPixmap(inference_path))
        else:
            widgets.inferenceWebcamText.setText(info)
            widgets.inferenceWebcamImage.setPixmap(QPixmap(inference_path))
        tts_text = info
    else:
        print("Error ocurred in the detection")
        widgets.inferenceText.setText("Error ocurred in the detection. Try again or check server connection.")
        tts_text = "Error ocurred in the detection. Try again or check server connection."


    if not client._closed:
        client.close()
    tts_engine.say(tts_text)
    tts_engine.runAndWait()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon("icon.ico"))
    window = MainWindow()
    sys.exit(app.exec())
