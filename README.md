# Image Descriptor with Jetson Nano 

## Graphic Interface
The user application was developed in Python. As mentioned above, it is based on PyDracula, to develop our GUI we add and modify the main project.

We use the OpenCV library to interact with the camera of the device where the application is installed. The "Open webcam" button allows you to turn on the webcam and view what the camera captures in the box on the left side of the screen. We can turn off the camera at any time by clicking the "Stop WebCam" button or simply closing the application. The "Inference" button takes pictures of what the WebCam is currently capturing and sends it to the Jetson Nano to process object identification. The last picture taken will be displayed in the frame on the right side of the screen.

![Image1](images/images/GUI_view_1.jpeg)

The second screen is accessed through the button with the code icon in the side menu that allows us to search and view the photos taken with the webCam and see its version after being processed by the object identification model and the text generation algorithm in the JetsonNano.

This view consists of a file explorer that only allows us to select files with a png, jpg and jpeg extension. In the box on the left shows the image and in the box on the right we will see the same image, but with the inferences of the Jetson Nano, the objects of the COCO dataset will be highlighted and a text will be generated describing the image. 

![Image2](images/images/GUI_view_2.jpeg)


